import React from 'react'
import { List, ListItem } from 'semantic-ui-react';
import { User } from '@daml.js/create-daml-app';
import { userContext } from './App';

type Props = {
  partyToAlias: Map<string, string>
}
/**
 * React component displaying the list of messages for the current user.
 */
const LikesList: React.FC<Props> = ({partyToAlias}) => {
  const likesResult = userContext.useStreamQueries(User.Likes);

  return (
    <List relaxed>
      {likesResult.contracts.map(likes => {
        const {sender, receiver, content} = likes.payload;
        return (
          <ListItem
            className='test-select-message-item'
            key={likes.contractId}>cotnt
            <strong>{partyToAlias.get(sender) ?? sender} &rarr; {partyToAlias.get(receiver) ?? receiver}:</strong> {content}
          </ListItem>
        );
      })}
    </List>
  );
};

export default LikesList;